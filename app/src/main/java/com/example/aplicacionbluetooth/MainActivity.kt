package com.example.aplicacionbluetooth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.util.Log
import android.widget.Button
import android.widget.Toast
import java.net.HttpURLConnection
import java.net.URL
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import android.os.StrictMode
import android.widget.TextView


class MainActivity : AppCompatActivity() {

    //var mBluetoohAdapter: BluetoothAdapter? =null;
    //var btnEnableDiscovery: Button ?=null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()

        StrictMode.setThreadPolicy(policy)

         var btnBuscar: Button = findViewById(R.id.button)
         var btnBluetooth: Button = findViewById(R.id.button2)
         var date = LocalDateTime.now()
         var formatter = DateTimeFormatter.ofPattern ("d-M-YYYY");
         var formatted = date.format(formatter)
         var mesAnt = formatted.split("-").get(1)
         var mesNuevo = mesAnt.toInt()-1
         var cadena = formatted.split("-")[0]+"-"+mesNuevo+"-"+formatted.split("-")[2]
         var txtOne: TextView = findViewById(R.id.txtUno)
         var txtTwo :TextView = findViewById(R.id.txtDos)
         //cadena = "9-7-2019"
         txtOne.setText("La fecha a buscar es: "+cadena)


         btnBuscar.setOnClickListener{
            Toast.makeText(this@MainActivity, cadena, Toast.LENGTH_SHORT).show()

             val response = try{
                 URL("https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/"+cadena+"/students/201621146")
                     .openStream().bufferedReader().use { it.readText() }

                 Toast.makeText(this@MainActivity, "Ya estás en la base de datos", Toast.LENGTH_LONG).show()
                 txtTwo.setText("Estado: Encontrado")
             } catch (e: Exception)
             {
                 Toast.makeText(this@MainActivity, "No se encontró en la base de datos", Toast.LENGTH_LONG).show()
                 txtTwo.setText("Estado: No Encontrado")
             }


         }

        btnBluetooth.setOnClickListener{
            var mBluetoothAdapter :BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

            if(mBluetoothAdapter.isDiscovering)
            {
                Toast.makeText(this@MainActivity,"Está en discovery",Toast.LENGTH_SHORT).show()
            }
            else
            {

                val enableBluetooth = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
                startActivityForResult(enableBluetooth,0)

                startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE),0)



                Toast.makeText(this@MainActivity,"Se activó el modo discovery",Toast.LENGTH_SHORT).show()

            }

        }





    }
}
